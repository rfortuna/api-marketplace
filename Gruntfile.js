module.exports = function (grunt) {

    var livereload = require('connect-livereload'),
        modRewrite = require('connect-modrewrite'),
        path = require('path');

    require('load-grunt-tasks')(grunt);

    var paths = {
        dist: 'dist'
    };

    grunt.initConfig({
        paths: paths,
        connect: {
            options: {
                port: process.env['PORT'] || 3002,
                hostname: '0.0.0.0'
            },
            livereload: {
                options: {
                    middleware: function (connect) {
                        return [
                            modRewrite(['^((?!\.css|\.js|\.html|\.eot|\.svg|\.ttf|\.woff|\.png|\.jpg|\.pdf|\.jpeg).)*$ /index.html [L]']),
                            livereload({port: 35730}),
                            connect.static(path.resolve('.tmp')),
                            connect.static(path.resolve('./'))
                        ];
                    }
                }
            }
        },
        watch: {
            options: {
                livereload: 35730
            },
            less: {
                files: [
                    'scripts/**/*.less',
                    'libs/**/*.less'
                ],
                tasks: ['less:develop']
            },
            template: {
                files: [
                    'scripts/*.tpl'
                ],
                tasks: ['template']
            },
            files: {
                files: [
                    'scripts/**/*.{js,html}',
                    '*.html',
                    'images/{,*/}*.{png,jpg,jpeg,gif,webp,svg}'
                ]
            }
        },
        clean: {
            options: {
                force: true
            },
            temp: {
                src: ['.tmp']
            },
            dist: {
                src: ['<%= paths.dist %>']
            }
        },
        useminPrepare: {
            src: ['index.html'],
            options: {
                dest: '<%= paths.dist %>'
            }
        },
        less: {
            develop: {
                files: {
                    ".tmp/scripts/content/layout.css": "scripts/content/layout.less"
                }
            },
            dist: {
                options: {
                    modifyVars: {
                        'fa-font-path': '"../fonts"',
                        'icon-font-path': '"../fonts/"',
                        'font-path': '"../fonts"'
                    }
                },
                files: {
                    ".tmp/scripts/content/layout.css": "scripts/content/layout.less"
                }
            }
        },
        template: {
            options: {
                data: {
                    api: process.env['APIURL'],
                    keycloak: process.env['KEYCLOAKURL'],
                    version: grunt.file.readJSON("package.json").version
                }
            },
            dist: {
                files: {
                    '.tmp/scripts/config.js': ['scripts/config.js.tpl']
                }
            }
        },
        requirejs: {
            // Options: https://github.com/jrburke/r.js/blob/master/build/example.build.js
            options: {
                name: 'main',
                baseUrl: 'scripts',
                mainConfigFile: 'scripts/main.js',
                out: '.tmp/scripts/main.js',
                optimize: 'none',
                almond: true,
                preserveLicenseComments: false,
                useStrict: true,
                wrap: true,
                findNestedDependencies: true,
                paths: {
                    config: '../.tmp/scripts/config'
                }
            },
            dist: {}
        },
        ngAnnotate: {
            options: {
                singleQuotes: true
            },
            dist: {
                files: {
                    '.tmp/scripts/main.js': ['.tmp/scripts/main.js']
                }
            }
        },
        autoprefixer: {
            dist: {
                expand: true,
                src: '.tmp/**/*.css'
            }
        },
        htmlmin: {
            dist: {
                options: {
                    //removeCommentsFromCDATA: true,
                    // https://github.com/yeoman/grunt-usemin/issues/44
                    collapseWhitespace: true
                    //collapseBooleanAttributes: true,
                    //removeAttributeQuotes: true,
                    //removeRedundantAttributes: true,
                    //useShortDoctype: true,
                    //removeEmptyAttributes: true,
                    //removeOptionalTags: true
                },
                files: [
                    {
                        expand: true,
                        cwd: '<%= paths.dist %>',
                        src: '*.html',
                        dest: '<%= paths.dist %>'
                    }
                ]
            }
        },
        imagemin: {
            dist: {
                files: [
                    {
                        expand: true,
                        cwd: 'images',
                        src: ['**/*.{png,jpg,gif}'],
                        dest: '<%= paths.dist %>/images'
                    }
                ]
            }
        },
        uglify: {
            options: {
                compress: {
                    sequences: false,
                    dead_code: false,
                    drop_debugger: false,
                    comparisons: false,
                    conditionals: false,
                    evaluate: false,
                    booleans: false,
                    loops: false,
                    if_return: false,
                    join_vars: false,
                    drop_console: false
                }
            },
            requirejs: {
                files: {
                    '<%= paths.dist %>/scripts/main.js': [
                        '.tmp/scripts/main.js'
                    ],
                    '<%= paths.dist %>/vendor/requirejs/require.js': [
                        'vendor/requirejs/require.js'
                    ]
                }
            }
        },
        copy: {
            dist: {
                files: [
                    {
                        expand: true,
                        dot: true,
                        cwd: '.',
                        dest: '<%= paths.dist %>',
                        src: [
                            '*.{ico,txt}',
                            'index.html'
                        ]
                    },
                    {
                        expand: true,
                        flatten: true,
                        dot: true,
                        cwd: '.',
                        dest: '<%= paths.dist %>/fonts',
                        src: [
                            'vendor/fontawesome/fonts/*.{otf,eot,svg,ttf,woff,woff2}',
                            'vendor/bootstrap/fonts/*.{otf,eot,svg,ttf,woff,woff2}',
                            'libs/pixeden-stroke-7-icon/fonts/*.{otf,eot,svg,ttf,woff,woff2}'
                        ]
                    }
                ]
            }
        },
        filerev: {
            dist: {
                src: [
                    '<%= paths.dist %>/scripts/**/*.js',
                    '<%= paths.dist %>/styles/**/*.css',
                    '<%= paths.dist %>/vendor/**/*.js'
                ]
            }
        },
        usemin: {
            html: ['<%= paths.dist %>/{,*/}*.html'],
            css: ['<%= paths.dist %>/styles/{,*/}*.css'],
            options: {
                dirs: ['<%= paths.dist %>']
            }
        }
    });

    grunt.registerTask('serve', [
        'clean:temp',
        'less:develop',
        'template',
        'connect:livereload',
        'watch'
    ]);

    grunt.registerTask('build', [
        'clean',
        'useminPrepare',
        'template',
        'less:dist',
        'requirejs',
        'concat',
        'ngAnnotate',
        'autoprefixer',
        'imagemin',
        'cssmin',
        'uglify',
        'copy',
        'filerev',
        'usemin',
        'htmlmin'
    ]);
};