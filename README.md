# API Marketplace front-end.

> Manage, secure and monitize your APIs and their dependencies from a single organized place.

This projects contains the front-end of the application.

## Usage

The project is using npm and bower to manage dependencies. For running and building GruntJS is used.

To start, first clone the repo:
```
$ git clone git@git.cloud.si:liis/api-marketplace-frontend.git
$ cd api-marketplace-frontend
```

Then install development dependencies:
```
$ npm install && bower install
```

To start the application:
```
$ npm start
```

By default the development server will be started on port `3002`. To change the port set the `PORT` environment
variable. Your browser will auto refresh if Grunt detects a file change. For example, to use `8000` as your development
port: 

```
$ PORT=8000 npm start
```

To run tests:
```
npm test
```

To build the application:
```
$ npm run build
```

You concatenated and minified files are located in the `dist` directory.

## Changelog

Recent changes can be viewed [here](https://git.cloud.si/liis/api-marketplace-frontend/blob/master/CHANGELOG.md).

## Contribute

See the [contributing docs](https://git.cloud.si/liis/api-marketplace-frontend/blob/master/CONTRIBUTING.md)

When submitting an issue, please follow the [guidelines](https://git.cloud.si/liis/api-marketplace-frontend/blob/master/CONTRIBUTING.md#bugs).

When submitting a bugfix, write a test that exposes the bug and fails before applying your fix. Submit the test alongside the fix.

When submitting a new feature, add tests that cover the feature.