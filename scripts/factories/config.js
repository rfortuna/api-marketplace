define([
    'config', 'lodash'
], function (config, _) {

    return function () {

        return _.assign({}, config, {
            passErrors: [400, 404, 422]
        });
    };
});