define([], function () {
    return /*@ngInject*/ function ($rootScope) {

        return {
           parseFilters : function(paramsArray) {
//                operation posibilities 
//                EQ, NEQ, LIKE, GT, GTE, LT, LTE, IN, NIN
                
                var queryString = "";
                
                angular.forEach(paramsArray, function(obj) {
                   
                   //string with fieldname 
                   queryString += obj.fieldName;
                   queryString += ":";
                   //string of operation from upper posibilities
                   queryString += obj.operation;
                   queryString += ":";
                   
                   if(typeof obj.value == 'string' || obj.value instanceof String) {
                       queryString += "'" +  obj.value + "'";
                   } else if(typeof obj.value == 'number' || obj.value instanceof Number){
                       queryString += obj.value;
                   } else if(obj.value instanceof Date || Object.prototype.toString.call(obj.value) === '[object Date]') {
                       queryString += "'" + obj.value.toISOString() + "'";
                   } else if(obj.value.constructor === Array || obj.value instanceof Array) {
                       queryString += "[" + obj.value.toString() + "]";
                   } else {
                       queryString += obj.value.toString();
                   }
                    
                   queryString += " ";
                    
                });
                
                return queryString.substring(0, queryString.length - 1);;
            } 
        }
    };
});