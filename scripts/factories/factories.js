define([
    'lodash', './config', './alerts', './queryUtils'
], function (_, Config, Alerts, QueryUtils) {

    var factories = {
        $config: Config,
        $alerts: Alerts,
        $queryUtils: QueryUtils
    };

    var initialize = function (angModule) {
        _.forEach(factories, function (factory, name) {
            angModule.factory(name, factory);
        });
    };

    return {
        initialize: initialize
    };
});