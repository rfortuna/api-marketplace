﻿define([
    'i18n!../nls/strings', './admin', 'text!./admin.html'
], function (strings, adminController, adminTemplate) {

    return {
        id: 'admin',
        url: '/admin',
        title: strings.states.admin.title,
        longTitle: strings.states.admin.longTitle,
        template: adminTemplate,
        controller: adminController,
        requiresAuth: true,
        requiredRoles: ['admin']
    };
});
