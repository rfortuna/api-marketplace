﻿define([
], function () {

    return /*@ngInject*/ function ($scope, toastr, User, ngTableParams, $filter) {

    	// Init settings
    	$scope.radioLayoutModel = 'Table';
    	$scope.filter = {
			        		username: ''
			        	};
    	
		// Watch for change in filter and get new data
    	$scope.$watch("filter", function(){
        	
        	User.all($scope.filter).$promise.then(function (data){      
	           	$scope.users = data;
	        
		   		$scope.tableParams = new ngTableParams({
			            page: 1,   // show first page
			            total: 1,  // value less than count hide pagination
			            count: 5,  // count per page
			            sorting: {
				            username: 'asc',
				            firstName: 'asc',
				            lastName: 'asc',
				            email: 'asc'     // initial sorting
				        },
				        filter: $scope.filter
			        }, {
		            	total: $scope.users.length, // length of data
			            getData: function($defer, params) {
				            
				            var filteredData = params.filter() ?
				                                $filter('filter')($scope.users,  params.filter()) :
				                                $scope.users;
				            $scope.users = params.sorting() ?
				                   				$filter('orderBy')(filteredData,params.orderBy()) :
				      	             			$scope.users;
							
							$defer.resolve($scope.users.slice((params.page() - 1) * params.count(), params.page() * params.count()));
			            }
			        });
		    	});
        	}, true);
    	
    };
});