﻿define([
    'i18n!../nls/strings', './dashboard', 'text!./dashboard.html'
], function (strings, dashboardController, dashboardTemplate) {

    return {
        id: 'dashboard',
        url: '/dashboard',
        title: strings.states.dashboard.title,
        longTitle: strings.states.dashboard.longTitle,
        template: dashboardTemplate,
        controller: dashboardController,
        menuItems: [
            {
                name: 'Dashboard'
            },
            {
                name: 'Analytics'
            },
            {
                name: 'Interface',
                children: [
                    {
                        name: 'Panels design',
                    },
                    {
                        name: 'Typography',
                    },
                    {
                        name: 'Colors',
                    },
                    {
                        name: 'Components',
                    },
                    {
                        name: 'Alerts',
                    },
                    {
                        name: 'Modals',
                    },
                    {
                        name: 'Draggable Panels',
                    },
                    {
                        name: 'List',
                    },
                    {
                        name: 'Tour',
                    },
                    {
                        name: 'Icons library',
                    },
                ]
            }
        ]
    };
});
