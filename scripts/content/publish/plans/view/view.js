﻿define([
], function () {

    return /*@ngInject*/ function ($scope, $rootScope) {
    	
    	$scope.createNewPlan = function(){
    		$scope.toggleFlow();
    		$rootScope.$state.go( 'layout.publish.plans.basic', '' );  		
    	}
    	
    };
});