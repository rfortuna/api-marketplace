﻿define([
    'i18n!../../../nls/strings', './view', 'text!./view.html'
], function (strings, viewController, viewTemplate) {
    return {
        id: 'view',
        url: '/view',
        title: strings.states.user.applications.title,
        longTitle: strings.states.user.applications.longTitle,
        template: viewTemplate,
        controller: viewController
    };
});
