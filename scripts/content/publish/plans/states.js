﻿define([
    'i18n!../../nls/strings', './plans', 'text!./plans.html', './basic/states', './restrictions/states', './view/states'
], function (strings, plansController, plansTemplate, basicStates, restrictionsStates, viewStates) {
    return {
        id: 'plans',
        url: '/plans',
        title: strings.states.user.applications.title,
        longTitle: strings.states.user.applications.longTitle,
        template: plansTemplate,
        controller: plansController,
        children: [basicStates, restrictionsStates, viewStates] 
    };
});
