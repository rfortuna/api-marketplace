﻿define([
], function () {

    return /*@ngInject*/ function ($scope, $rootScope) {
    	$scope.next = function(){
    		$rootScope.$state.go( 'layout.publish.plans.restrictions', '');
    	}
    	
    	$scope.prev = function(){
    		$rootScope.$state.go( 'layout.publish.plans.basic', '');
    	}
    	
    	$scope.complete = function(){
    		$scope.toggleFlow();
    		$rootScope.$state.go( 'layout.publish.plans.view', '' );
    	}
    	
    	$scope.back = function(){
    		$scope.toggleFlow();
    		$rootScope.$state.go( 'layout.publish.plans.view', '' );
    	}
    	
    };
});