﻿define([], function() {

	return /*@ngInject*/function($scope, ngTableParams, $sce) {

		var data = [ {type: "Requests", limit: "10000 requests", unit: "Month"},
		             {type: "Requests", limit: "1000 requests", unit: "Day"},
		             {type: "Bandwith", limit: "200 MB", unit: "Month"}];

		$scope.restrictionsTableParams = new ngTableParams({
			page : 1, // show first page
			count : 10
		// count per page
		}, {
			total : data.length, // length of data
			getData : function($defer, params) {
				$defer.resolve(data.slice((params.page() - 1) * params.count(),
						params.page() * params.count()));
			}
		});
		
	};
});