﻿define([
    'i18n!../../../nls/strings', './restrictions', 'text!./restrictions.html'
], function (strings, restrictionsController, restrictionsTemplate) {
    return {
        id: 'restrictions',
        url: '/restrictions',
        title: strings.states.user.applications.title,
        longTitle: strings.states.user.applications.longTitle,
        template: restrictionsTemplate,
        controller: restrictionsController
    };
});
