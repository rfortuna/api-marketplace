﻿define([
    'i18n!../../../nls/strings', './basic', 'text!./basic.html'
], function (strings, basicController, basicTemplate) {
    return {
        id: 'basic',
        url: '/basic',
        title: strings.states.user.applications.title,
        longTitle: strings.states.user.applications.longTitle,
        template: basicTemplate,
        controller: basicController
    };
});
