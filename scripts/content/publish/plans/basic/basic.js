﻿define([
], function () {

    return /*@ngInject*/ function ($scope, ngTableParams, $sce, $filter, $q) {
    	
    	// feature table
    	
    	var data = [{name: "Feature 1"},
    	            {name: "Feature 2"},
    	            {name: "Feature 3"},
    	            {name: "Feature 4"},
    	            {name: "Feature 5"},
    	            {name: "Feature 6"}];

    	        $scope.featureTableParams = new ngTableParams({
    	            page: 1,            // show first page
    	            count: 10           // count per page
    	        }, {
    	            total: data.length, // length of data
    	            getData: function($defer, params) {
    	                $defer.resolve(data.slice((params.page() - 1) * params.count(), params.page() * params.count()));
    	            }
    	        });
    	
    	//endpoints
    	
    	var dataEp = [{id: 1, name: "Endpoint 1", role: 'Group 1'},
    	            {id: 2, name: "Endpoint 2", role: 'Group 1'},
    	            {id: 3, name: "Endpoint 3", role: 'Group 1'},
    	            {id: 4, name: "Endpoint 4", role: 'Group 2'},
    	            {id: 5, name: "Endpoint 5", role: 'Group 2'},
    	            {id: 6, name: "Endpoint 6", role: 'Group 3'},
    	            {id: 7, name: "Endpoint 7", role: 'Group 4'},
    	            {id: 8, name: "Endpoint 8", role: 'Group 4'}];

    	
        $scope.tableParams = new ngTableParams({
            page: 1,            // show first page
            count: 10           // count per page
        }, {
        	groupBy: 'role',
            total: dataEp.length, // length of data
            getData: function($defer, params) {
                // use build-in angular filter
                var orderedData = params.sorting() ?
                        $filter('orderBy')(dataEp, params.orderBy()) :
                        dataEp;
                orderedData = params.filter() ?
                        $filter('filter')(orderedData, params.filter()) :
                        orderedData;

                params.total(orderedData.length); // set total for recalc pagination
                $defer.resolve($scope.users = orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
            }
        });
  
        var getGroups = function(){
        	var groups = [];
        	angular.forEach($scope.users, function(item) {
        		 if($.inArray(item.role, groups) < 0)
        			 groups.push(item.role);
        	});
        	return groups;
        };
        
        var inArray = Array.prototype.indexOf ?
                function (val, arr) {
                    return arr.indexOf(val)
                } :
                function (val, arr) {
                    var i = arr.length;
                    while (i--) {
                        if (arr[i] === val) return i;
                    }
                    return -1
                };
        $scope.names = function(column) {
            var def = $q.defer(),
                    arr = [],
                    names = [];
            angular.forEach(dataEp, function(item){
                if (inArray(item.name, arr) === -1) {
                    arr.push(item.name);
                    names.push({
                        'id': item.name,
                        'title': item.name
                    });
                }
            });
            def.resolve(names);
            return def;
        };

        $scope.checkboxes = { 'checked': false, items: {} };

        // watch for check all checkbox
        $scope.$watch('checkboxes.checked', function(value) {
//            angular.forEach($scope.users, function(item) {
//            	//console.log(item);
//                if (angular.isDefined(item.id)) {
//                    $scope.checkboxes.items[item.id] = value;
//                }
//            });
            angular.forEach(getGroups(), function(item){
            		 $scope.checkboxes.items[item] = value;
            		 angular.forEach($scope.users, function(item_in) {
                     	//console.log(item);
                         if (item == item_in.role) {
                             $scope.checkboxes.items[item_in.id] = value;
                         }
                     });
            	
            });
        });

        // watch for data checkboxes
        $scope.$watch('checkboxes.items', function(values) {
            if (!$scope.users) {
                return;
            }
//            console.log(values);
//            angular.forEach(getGroups(), function(item){
//            	var value = false;
//            	if($.inArray(item, values) < 0){
//            		if(angular.isDefined(item.id))
//            			value = true;
//            	}else{
//            		
//            		value = !values[item];
//            	}
//            	console.log(value);
//            	 angular.forEach($scope.users, function(item_in) {
//                  	//console.log(item);
//                      if (item == item_in.role) {
//                    	  console.log(item_in);
//                          $scope.checkboxes.items[item_in.id] = value;
//                      }
//                  });
//            });
            
                        
            var checked = 0, unchecked = 0,
                    total = $scope.users.length;
            angular.forEach($scope.users, function(item) {
                checked   +=  ($scope.checkboxes.items[item.id]) || 0;
                unchecked += (!$scope.checkboxes.items[item.id]) || 0;
            });
            
            
            if ((unchecked == 0) || (checked == 0)) {
                $scope.checkboxes.checked = (checked == total);
            }
            // grayed checkbox
            angular.element(document.getElementById("select_all")).prop("indeterminate", (checked != 0 && unchecked != 0));
        }, true);
    	

   
    
    };
});