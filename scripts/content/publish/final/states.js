﻿define([
    'i18n!../../nls/strings', './final', 'text!./final.html'
], function (strings, finalController, finalTemplate) {
    return {
        id: 'final',
        url: '/final',
        title: strings.states.user.applications.title,
        longTitle: strings.states.user.applications.longTitle,
        template: finalTemplate,
        controller: finalController
    };
});
