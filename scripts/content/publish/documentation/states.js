﻿define([
    'i18n!../../nls/strings', './documentation', 'text!./documentation.html'
], function (strings, documentationController, documentationTemplate) {
    return {
        id: 'documentation',
        url: '/documentation',
        title: strings.states.user.applications.title,
        longTitle: strings.states.user.applications.longTitle,
        template: documentationTemplate,
        controller: documentationController
    };
});
