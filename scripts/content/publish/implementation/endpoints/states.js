﻿define([
    'i18n!../../../nls/strings', './endpoints', 'text!./endpoints.html'
], function (strings, endpointsController, endpointsTemplate) {
    return {
        id: 'endpoints',
        url: '/endpoints',
        title: strings.states.user.applications.title,
        longTitle: strings.states.user.applications.longTitle,
        template: endpointsTemplate,
        controller: endpointsController
    };
});
