﻿define([
	'text!./modal.html'
], function (modalTemplate) {

    return /*@ngInject*/ function ($scope, $modal, ngTableParams) {
        
    	//modal
    	$scope.modalController = function($scope, $modalInstance) {

            $scope.ok = function() {
                $modalInstance.close();
            };

            $scope.cancel = function() {
                $modalInstance.dismiss('cancel');
            };
        };

        $scope.showModal = function() {
            var modalInstance = $modal.open({
                animation: false,
                template: modalTemplate,
                controller: $scope.modalController
            });
        };
        
        
        $scope.queryParamTable = new ngTableParams({
            page: 1,   // show first page
            total: 1,  // value less than count hide pagination
            count: 5  // count per page
        }, {
            counts: [], // hide page counts control
            getData: function($defer, params) {
                $defer.resolve($scope.groups.slice((params.page() - 1) * params.count(), params.page() * params.count()));
            }
        });
        
        
        $scope.selectedEndpoint = null;
       
        $scope.setSelectedEndpoint = function(gid, eid){
        	$scope.selectedEndpoint = angular.copy($scope.groups[gid - 1].endpoints[eid -  1]);
        	console.log($scope.selectedEndpoint);
        };
  
    };
});