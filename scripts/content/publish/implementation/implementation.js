﻿define([
], function () {

    return /*@ngInject*/ function ($scope, $rootScope) {

    	$scope.groups = [
    	                 {"id": "1", "name":"Group 1", "description":"description...", 
    	                	 "endpoints" :[
    	                	               {"id": "1", "name": "endpoint 1", "path": "/path", "method": "POST", "group": "Group 1",
    	                	            	   "parameters": [{"name": "name", "type": "type", "condition": "condition", "description": "desc"},
    	                	            	                  {"name": "name1", "type": "type", "condition": "condition", "description": "desc"}]
    	                	               },
    	                	               {"id": "2", "name": "endpoint 2", "path": "/path", "method": "POST", "group": "Group 1",
    	                	            	   "parameters": [{"name": "name", "type": "type", "condition": "condition", "description": "desc"},
    	                	            	                  {"name": "name1", "type": "type", "condition": "condition", "description": "desc"}]
    	                	               },
    	                	               {"id": "3", "name": "endpoint 3", "path": "/path", "method": "POST", "group": "Group 1",
    	                	            	   "parameters": [{"name": "name", "type": "type", "condition": "condition", "description": "desc"},
    	                	            	                  {"name": "name1", "type": "type", "condition": "condition", "description": "desc"}]
    	                	               }
    	                	               ]},
    	                 {"id": "2", "name":"Group 2", "description":"description...", 
    	                	 "endpoints" :[
    	                	               {"id": "1", "name": "endpoint 1", "path": "/path", "method": "POST", "group": "Group 1",
    	                	            	   "parameters": [{"name": "name", "type": "type", "condition": "condition", "description": "desc"},
    	                	            	                  {"name": "name1", "type": "type", "condition": "condition", "description": "desc"}]
    	                	               },
    	                	               {"id": "2", "name": "endpoint 2", "path": "/path", "method": "POST", "group": "Group 1",
    	                	            	   "parameters": [{"name": "name", "type": "type", "condition": "condition", "description": "desc"},
    	                	            	                  {"name": "name1", "type": "type", "condition": "condition", "description": "desc"}]
    	                	               }
    	                	               ]},
    	                 {"id": "3", "name":"Group 3","description":"description...", 
    	                	 "endpoints" :[
    	                	               {"id": "1", "name": "endpoint 1", "path": "/path", "method": "POST", "group": "Group 1",
    	                	            	   "parameters": [{"name": "name", "type": "type", "condition": "condition", "description": "desc"},
    	                	            	                  {"name": "name1", "type": "type", "condition": "condition", "description": "desc"}]
    	                	               },
    	                	               {"id": "2", "name": "endpoint 3", "path": "/path", "method": "POST", "group": "Group 1",
    	                	            	   "parameters": [{"name": "name", "type": "type", "condition": "condition", "description": "desc"},
    	                	            	                  {"name": "name1", "type": "type", "condition": "condition", "description": "desc"}]
    	                	               }
    	                	               ]}
    	];
    	
    	$scope.changeTab = function(id){
    		if(id == 1 ){
    			$("#group").removeClass("active");
    			$("#endpoint").addClass("active");
    			$rootScope.$state.go( "layout.publish.implementation.endpoints", "" );
    		}else{
    			$("#group").addClass("active");
    			$("#endpoint").removeClass("active");
    			$rootScope.$state.go( "layout.publish.implementation.groups", "" );
    		}
    	}
    };
});