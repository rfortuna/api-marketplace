﻿define([
], function () {

    return /*@ngInject*/ function ($scope, toastr, ngTableParams) {
    	$scope.isNew = false;
    	

    	$scope.selected = angular.copy($scope.groups[0]);
    	

        $scope.groupTable = new ngTableParams({
            page: 1,   // show first page
            total: 1,  // value less than count hide pagination
            count: 5  // count per page
        }, {
            counts: [], // hide page counts control
            getData: function($defer, params) {
                $defer.resolve($scope.groups.slice((params.page() - 1) * params.count(), params.page() * params.count()));
            }
        });
        
        $scope.setSelected = function(id){
        	if($scope.groups.length > 0){
        		$scope.selected = angular.copy($scope.groups[id - 1]);
        	}
        };
        
        $scope.saveGroup = function(){
        	if(!$scope.isNew){
        		$scope.groups[$scope.selected.id - 1] = angular.copy($scope.selected);
        		toastr.success('Changes saved',''); 
        		}
        	else{
        		$scope.isNew = false;
        		$scope.selected.id = $scope.groups.length;
        		$scope.groups.push($scope.selected);
            	$scope.selected = angular.copy($scope.groups[$scope.groups.length - 1]);
            	toastr.success('Success', 'New group added.');
        	}
        };
        
        $scope.resetGroup = function(){
        	$scope.selected = null;
        	if($scope.isNew)
        		$scope.isNew = false;
        };
        
        $scope.newGroup = function(){
        	$scope.isNew = true;
        	$scope.selected = null;
        };
    };
});