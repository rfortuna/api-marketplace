﻿define([
    'i18n!../../../nls/strings', './groups', 'text!./groups.html'
], function (strings, groupsController, groupsTemplate) {
    return {
        id: 'groups',
        url: '/groups',
        title: strings.states.user.applications.title,
        longTitle: strings.states.user.applications.longTitle,
        template: groupsTemplate,
        controller: groupsController
    };
});
