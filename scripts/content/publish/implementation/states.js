﻿define([
    'i18n!../../nls/strings', './implementation', 'text!./implementation.html', './groups/states', './endpoints/states'
], function (strings, implementationController, implementationTemplate, groupsStates, endpointsStates) {
    return {
        id: 'implementation',
        url: '/implementation',
        title: strings.states.user.applications.title,
        longTitle: strings.states.user.applications.longTitle,
        template: implementationTemplate,
        controller: implementationController,
        children: [groupsStates, endpointsStates]  
    };
});
