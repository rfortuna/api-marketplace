﻿define([
    'i18n!../nls/strings', './publish', 'text!./publish.html', './settings/states', './plans/states', 
    './documentation/states', './implementation/states', './final/states'
], function (strings, publishController, layoutTemplate, settingsStates, plansStates, documentationStates, implementationStates, finalStates) {
    return {
        id: 'publish',
        url: '/publish',
        title: strings.states.layout.title,
        longTitle: strings.states.layout.longTitle,
        template: layoutTemplate,
        controller: publishController,
        children: [settingsStates, plansStates, documentationStates, implementationStates, finalStates]  
    };
});
