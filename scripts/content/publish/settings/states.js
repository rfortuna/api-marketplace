﻿define([
    'i18n!../../nls/strings', './settings', 'text!./settings.html'
], function (strings, settingsController, settingsTemplate) {
    return {
        id: 'settings',
        url: '/settings',
        title: strings.states.user.applications.title,
        longTitle: strings.states.user.applications.longTitle,
        template: settingsTemplate,
        controller: settingsController
    };
});
