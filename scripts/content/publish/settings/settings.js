﻿define([
	'text!./modal.html'
], function (modalTemplate) {

    return /*@ngInject*/ function ($scope, $modal, Publish) {
    	
    	//logo upload modal
    	$scope.modalController = function($scope, $modalInstance) {

            $scope.ok = function() {
                $modalInstance.close();
            };

            $scope.cancel = function() {
                $modalInstance.dismiss('cancel');
            };
        };

        $scope.showModalLogo = function() {
            var modalInstance = $modal.open({
                animation: false,
                template: modalTemplate,
                controller: $scope.modalController
            });
        };
    	
    	
        $scope.test = function(){
        	console.log($scope.api);
        }
        
        
    	$scope.post = function(){
//    		console.log($scope.user);
//    		console.log($stateParams.user);
    		Publish.createApi({id: 1});
    	}
    	
    	$scope.createApi = function(){
    		Publish.createApi();
//            User.saveOrganisationWithOwner({id: $scope.user.id}, organisation).$promise
//                .then(function(data) {
//                    $state.go('^.list');
//                });
        }
    	
    };
});