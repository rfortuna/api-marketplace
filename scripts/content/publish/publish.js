﻿define([
], function () {

    return /*@ngInject*/ function ($scope, $rootScope) {
    	
    	var prevButton = "tab1";
    	var current = 0;
    	$scope.visibleTab = "tab1";
    	var tabs = ["settings", "implementation.endpoints", "plans.view", "documentation", "final"];
    	
    	$scope.changeTab = function(tabID, cur){
    		// change buttons css
    		$("#"+prevButton).removeClass("btn-primary");
    		$("#"+prevButton).addClass("btn-default");
    		$("#"+tabID).removeClass("btn-default");
    		$("#"+tabID).addClass("btn-primary");
    		//change tabs
    		
    		$scope.visibleTab = tabID;
    		prevButton = tabID;
    		current = cur;
    		
    		$rootScope.$state.go( 'layout.publish.'+tabs[current], "" );
    	}

    	$scope.move = function(direction){
    	    		
    		// change buttons css
    		tabID = "#tab"+(current+1);
    		$(tabID).removeClass("btn-primary");
    		$(tabID).addClass("btn-default");
    		if (direction)
    			current += 1;
    		else
    			current -= 1;
    		tabID = "#tab"+(current+1);
    		$(tabID).removeClass("btn-default");
    		$(tabID).addClass("btn-primary");
    		//change tabs
    		
    		$scope.visibleTab = tabID;
    		prevButton = tabID;
    		$rootScope.$state.go( 'layout.publish.'+tabs[current], "" );
    	}
    	
    	$scope.changeSelect = function(id){
    		$("#"+id).toggleClass("checked");
    	}
    	
    	$scope.toggleFlow = function(){
    		$("#tab1").toggleClass("disabled");
    		$("#tab2").toggleClass("disabled");
    		$("#tab3").toggleClass("disabled");
    		$("#tab4").toggleClass("disabled");
    		$("#tab5").toggleClass("disabled");
    	}
    	
    	if($rootScope.$state.$current.absoluteId.search(tabs[current]) < 0){
    		current = 0;
    		$rootScope.$state.go( 'layout.publish.'+tabs[current], "" );
    	}
    	
    };
});