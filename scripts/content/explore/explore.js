﻿define(['lodash', 'i18n!../nls/strings'], function (_, strings) {

    return /*@ngInject*/ function ($scope, Producer, Category, Organisation, User, $timeout, $queryUtils, $state) {

        $scope.$strings = _.merge( {}, strings, $scope.$strings.states.explore );

        console.log($scope.$strings);

        var mockColors = ["#2fdddd", "#a3d39c", "#92c2ff",  "#f2aaff"];

        $scope.radioFilterModel = 'All';
        $scope.radioLayoutModel = 'Grid';


        var infiniteScrollCalled = true;
        var infiniteScrollEnabled = true;

        $scope.filter = {
            limit: 20,
            categories: []
        };

        $scope.producers = [];

        $scope.categories = [];

        Category.query().$promise.then(function(data){
            $scope.categories = data;
        });

        var getNewProducers = function() {
            $scope.producers = [];

            //enable infinite scroll, cause it is starting from zero
            infiniteScrollEnabled = true;

            getMoreProducers();
        }

        var getMoreProducers = function(){

            var categories = _.map($scope.filter.categories, function(obj) {
                return obj.id;
            });

            Producer.query({
                 offset: $scope.producers.length,
                 limit: $scope.filter.limit,
                 categories: categories
//                 where: $queryUtils.parseFilters([{
//                     fieldName: "categories",
//                     operation: "IN",
//                     value: categories
//                 }])
                 }).$promise
            .then(function(data) {

    	          if(data.length == 0) {
                    //retrieved all data, turn infinite scroll off
                    infiniteScrollEnabled = false;
                    return;
                }

                //add colors for categories
                //create colors for categories
                angular.forEach(data, function(obj) {
                    angular.forEach(obj.categories, function(category) {
                        category.color = mockColors[Math.floor(Math.random()*4)];
                    });
                });

                $scope.producers.push.apply($scope.producers, data);

                infiniteScrollCalled = false;

                var organisationsId = _.map(data, function(obj) {
                    if (obj.organisation != undefined ){
                        return obj.organisation.id;
                    }
                });

                Organisation.query({
                    where: $queryUtils.parseFilters([
                      {
                        fieldName: "id",
                        operation: "IN",
                        value: organisationsId
                      }
                    ])
                }).$promise.then(function(data) {

                    $scope.organisations = _.indexBy(data, 'id');

                });

                var usersId = _.map(data, function(obj) {
                  if (obj.user != undefined ){
                      return obj.user.id;
                  }
                });

                User.query({
                  where : $queryUtils.parseFilters([
                    {
                      fieldName: "id",
                      operation: "IN",
                      value: usersId
                    }
                  ])
                }).$promise.then(function(data){
                  $scope.users = _.indexBy(data, 'id');
                });

            });

        };

        $scope.pagingFunction = function() {
            // poklici api in nalozi nove elemente
            if(!infiniteScrollCalled && infiniteScrollEnabled) {
                infiniteScrollCalled = true;
                getMoreProducers();
            }
        }

        $scope.$watch("filter.categories", function(){

            $timeout(function(){
                $(".category-search .ui-select-match").children().each(function(index, obj){
                    $(obj).children().css("border-color","red");
                });
            });
            getNewProducers();

        });

        $scope.$watch("filter.sortBy", function(){

            //getNewProducers();

        });

        $scope.goToDetails = function(id) {
            console.log("show details");
            $state.go('layout.apiDetails', {producerId: id})
        }


    };
});
