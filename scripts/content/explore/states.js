﻿define([
    'i18n!../nls/strings', './explore', 'text!./explore.html'
], function (strings, exploreApisController, exploreApisTemplate) {
    return {
        id: 'explore',
        url: "/",
        title: strings.states.explore.title,
        longTitle: strings.states.explore.longTitle,
        template: exploreApisTemplate,
        controller: exploreApisController
    };
});
