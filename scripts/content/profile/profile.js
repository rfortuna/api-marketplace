﻿define([
], function () {

    return /*@ngInject*/ function ($scope, $rootScope, $q, $timeout) {
    	
    	console.log($rootScope.$auth.keycloak);
		
    	$scope.goToProfile = false;
    	$scope.readOnly = false;
    	$rootScope.$auth.keycloak.loadUserProfile().success( function (userProfile) {
			$scope.user = userProfile;
			$scope.goToProfile = true;
			$scope.$apply();
		});
    };
});