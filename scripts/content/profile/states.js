﻿define([
    'i18n!../nls/strings', './profile', 'text!./profile.html'
], function (strings, profileController, profileTemplate) {

    return {
        id: 'profile',
        url: '/profile',
        title: strings.states.profile.title,
        longTitle: strings.states.profile.longTitle,
        template: profileTemplate,
        controller: profileController,
        requiresAuth: true
    };
});
