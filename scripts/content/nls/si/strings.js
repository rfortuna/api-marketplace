define({

    states: {
        layout: {
            title: 'Predloga',
            longTitle: 'Predloga aplikacije'
        },
        dashboard: {
            title: 'Namizje',
            longTitle: 'Namizje'
        },
        explore: {
            title: 'Pregled API-jev',
            longTitle: 'Pregled API-jev',
            sort: {
                placeholder: 'Razporedi po:',
                rating: 'Ocena',
                location: 'Lokacija',
                popularity: 'Priljubljenost',
                publisher: 'Organizacija'
            }
        }
    },
    errors : {
        noAuthorization: 'Za ogled te strani nimate dovoljenja.',
    },
    warnings: {
         noAuthentication: 'Prosimo, prijavite se za nadaljevanje.'
    }
});