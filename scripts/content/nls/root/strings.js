define({

    states: {
        layout: {
            title: 'Layout',
            longTitle: 'Application layout'
        },
        dashboard: {
            title: 'Dashboard',
            longTitle: 'Dashboard'
        },
        apiDetails: {
            title: 'API details',
            longTitle: 'API details'
        },
        explore: {
            title: 'Explore APIs',
            longTitle: 'Explore APIs',
            sort: {
                placeholder: 'Sort by',
                rating: 'Rating',
                location: 'Location',
                popularity: 'Popularity',
                publisher: 'Publisher'
            }
        },
        organisation: {
            title: 'Organisations',
            longTitle: 'Organisations',
            name : 'Name',
            description: 'Description',
            address: 'Address',
            vatNumber: 'Vat number',
            country: 'Country',
            edit:{
                headerMessage: 'Edit organisation',
                submitMessage: 'Update'
            },
            create:{
                headerMessage: 'Create organisation',
                submitMessage: 'Create'
            },
            details:{
                editMessage: 'Edit',
                joinOrganisation: 'Join'
            }
        },
        admin: {
            title: 'Sample page',
            longTitle: 'Sample page visible only to admin'  
        },
        user: {
            title: 'User',
            longTitle: 'User',
            apis: {
                title: 'APIs',
                longTitle: 'APIs'
            },
            applications: {
                title: 'Applications',
                longTitle: 'Applications'
            },
            organisations: {
                title: 'Organisations',
                longTitle: 'Organisations'
            }
        },
        profile: {
            title: 'Profile',
            longTitle: 'My Profile'
        },
        application: {
            title: 'Application',
            longTitle: 'Application'
        }
    },
    errors : {
        noAuthorization: 'You do not have permisions to visit that page.',
    },
    warnings: {
         noAuthentication: 'Please login before continuing.'
    }
});