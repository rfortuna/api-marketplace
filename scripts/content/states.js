﻿define([
    'i18n!./nls/strings', './layout', 'text!./layout.html', './dashboard/states', './apiDetails/states',
    './explore/states', './admin/states', './user/states', './organisation/states', './profile/states',
    './publish/states', './application/states'
], function (strings, layoutController, layoutTemplate, dashboardStates, apiDetailsStates,
             exploreApisStates, adminStates, userStates, organisationStates, profileStates,
             publishStates, applicationStates) {

    return {
        id: 'layout',
        url: '',
        title: strings.states.layout.title,
        longTitle: strings.states.layout.longTitle,
        abstract: true,
        visible: false,
        template: layoutTemplate,
        controller: layoutController,
        children: [
            dashboardStates,
            apiDetailsStates,
            exploreApisStates,
            adminStates,
            userStates,
            organisationStates,
            profileStates,
            publishStates,
            applicationStates
        ]
    };
});