﻿define([
], function () {

    return /*@ngInject*/ function ($scope, $state) {

        $scope.getStatePath = function () {
            var currentState = $scope.$state.current;
            var depth = currentState.depth;
            var statePath = [currentState];
            for (i = 0; i < depth; i++) {
                currentState = currentState.parentState;
                statePath.push(currentState);
            }
            return statePath.reverse();
        };

        $scope.isValidLink = function (state) {
            if (!state.abstract && !state.hasParams && !state.visible && !($scope.isMobileDevice() && state.children[0].requiresAuth)) return true;
            else return !!(state.abstract && state.children && !state.hasParams && state.children[0] && !state.children[0].abstract && !state.children[0].hasParams && state.children[0].visible && !($scope.isMobileDevice() && state.children[0].requiresAuth));
        };

        $scope.goToState = function (state) {
            if (!state.abstract && !state.hasParams && !state.visible && !($scope.isMobileDevice() && state.children[0].requiresAuth)) $state.transitionTo(state.absoluteId);
            else if (state.abstract && state.children && !state.hasParams && state.children[0] && !state.children[0].abstract && !state.children[0].hasParams && state.children[0].visible && !($scope.isMobileDevice() && state.children[0].requiresAuth))
                $state.transitionTo(state.children[0].absoluteId);
        };

        $scope.login = function () {
            // Use keycloak login
            $scope.$auth.keycloak.login();
        };

        $scope.logout = function () {
            // Use keycloak logout
            $scope.$auth.keycloak.logout();
        };

    };
});