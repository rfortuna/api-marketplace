define([
    'text!./modal.html'
], function (modalTemplate) {

    return /*@ngInject*/ function ($scope, $modal, User) {

        $scope.modalController = function($scope, $modalInstance) {

            $scope.ok = function() {
                $modalInstance.close();
            };

            $scope.cancel = function() {
                $modalInstance.dismiss('cancel');
            };
        };

        $scope.showModal = function() {
            var modalInstance = $modal.open({
                animation: false,
                template: modalTemplate,
                controller: $scope.modalController
            });
        };

        User
            .getOrganisations({
                id: $scope.user.id
            })
            .$promise
            .then(function(data) {
                $scope.organisations = data;
            });
    };
});