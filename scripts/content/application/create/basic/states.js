define([
    'i18n!../../../nls/strings', './basic', 'text!./basic.html'
], function (strings, basicController, basicTemplate) {
    return {
        id: 'basic',
        url: '',
        title: strings.states.application.title,
        longTitle: strings.states.application.longTitle,
        template: basicTemplate,
        controller: basicController
    };
});
