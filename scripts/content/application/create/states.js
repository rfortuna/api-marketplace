define([
    'i18n!../../nls/strings', './create', 'text!./create.html',
    './basic/states', './version/states'
], function (strings, createController, createTemplate, basicStates, versionStates) {
    return {
        id: 'create',
        url: '/create',
        abstract: true,
        title: strings.states.application.title,
        longTitle: strings.states.application.longTitle,
        template: createTemplate,
        controller: createController,
        children: [
            basicStates,
            versionStates
        ]
    };
});
