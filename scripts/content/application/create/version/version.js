define([
], function () {

    return /*@ngInject*/ function ($scope, VersionType) {

        VersionType
            .getAll()
            .$promise
            .then(function(data) {
                $scope.versionTypes = data;
            });
    };
});