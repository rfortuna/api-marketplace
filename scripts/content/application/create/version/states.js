define([
    'i18n!../../../nls/strings', './version', 'text!./version.html'
], function (strings, versionController, versionTemplate) {
    return {
        id: 'version',
        url: '/version',
        title: strings.states.application.title,
        longTitle: strings.states.application.longTitle,
        template: versionTemplate,
        controller: versionController
    };
});
