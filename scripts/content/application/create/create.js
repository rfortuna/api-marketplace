define([
], function () {

    return /*@ngInject*/ function ($scope, $state, User) {

        // Initial user
        $scope.user = {
            id: 1
        };

        // Form data
        $scope.consumer = {
            name: '',
            description: '',
            organisation: {

            },
            consumerVersions: [
                {
                    isDefault: true,
                    versionType: {

                    }
                }
            ]
        }

        // Initial step
        $scope.step = 0;

        // Wizard functions
        $scope.wizard =  {
            show: function(number) {
                $scope.step = number;
                var selectedState = $state.current.parentState.children[$scope.step];
                if (selectedState !== undefined)
                    $state.go(selectedState.absoluteId);
            },
            next: function(valid) {
                if (!valid)
                    return;

                $scope.step++;
                var nextState = $state.current.parentState.children[$scope.step];
                if (nextState !== undefined)
                    $state.go(nextState.absoluteId);
            },
            prev: function(valid) {
                $scope.step-- ;
                var prevState = $state.current.parentState.children[$scope.step];
                if (prevState !== undefined)
                    $state.go(prevState.absoluteId);
            },
            submit: function(valid) {
                if (!valid)
                    return;

                User
                    .createConsumer({id: $scope.user.id}, $scope.consumer)
                    .$promise;
            }
        };
    };
});
