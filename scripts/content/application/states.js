define([
    'i18n!../nls/strings', './application', 'text!./application.html',
    './create/states'
], function (strings, applicationController, applicationTemplate,
             createStates, versionStates) {
    return {
        id: 'application',
        url: '/application',
        title: strings.states.application.title,
        longTitle: strings.states.application.longTitle,
        template: applicationTemplate,
        controller: applicationController,
        children: [
            createStates
        ]
    };
});
