﻿define([
    'i18n!../nls/strings', './apiDetailsController', 'text!./apiDetails.html'
], function (strings, apiDetailsController, apiDetailsTemplate) {

    return {
        id: 'apiDetails',
        url: '/apis/{producerId}?page&version',
        title: strings.states.apiDetails.title,
        longTitle: strings.states.apiDetails.longTitle,
        template: apiDetailsTemplate,
        controller: apiDetailsController,
        reloadOnSearch: false,
<<<<<<< Updated upstream
        menuItems: [
            {
                name: 'Overview'
            },
            {
                name: 'API Details'
            },
            {
                name: 'Announcements',
            },
            {
                name: 'Requirements'
            },
            {
                name: 'Location'
            }
        ]
=======
        
>>>>>>> Stashed changes
    };
});
