﻿define([
], function () {

    return /*@ngInject*/ function ($scope, $state, $stateParams, $location, Producer, toastr, Organisation) {
        
        var reportError = function() {
            toastr.error('Please try again later.', 'Something went wrong!');
            $state.go('^.dashboard');
        };
        
        // set initial version from query param
        $scope.initialVersion = $stateParams.version;
        
        // set producer id
        $scope.producerId = $stateParams.producerId;
        
        // get producer details and versions
        Producer.getApiDetails({id: $stateParams.producerId}).$promise.then(function(data) {
            $scope.apiDetails = data;
            
            // get all versions
            Producer.getVersions({id: $stateParams.producerId}).$promise.then(function(data) {
                $scope.apiDetails.versions = data;
            }, function() {
                reportError();
            });
            
            // get organisation details
            Organisation.get({id: $scope.apiDetails.organisation.id}).$promise.then(function(org) {
               $scope.apiDetails.organisation = org; 
            }, function() {
                reportError();
            });
            
        }, function() {
            reportError();
        });
        
        // load overview according to version ID.
        $scope.loadOverview = function(versionId) {
            Producer.getOverview({producerVersionId: versionId},
                {id: $stateParams.producerId}).$promise.then(function(data) {
                $scope.markdown = data.overview;
            }, function() {
                toastr.error('Unable to show API overview.', 'Something went wrong!');
            });
        };
        
        // load details (code samples, markdown) according to version ID.
        $scope.loadDocumentation = function(versionId) {
          Producer.getDetails({producerVersionId: versionId},
              {id: $stateParams.producerId}).$promise.then(function(data) {
                  $scope.markdown = data.details;
              }, function() {
                  toastr.error('Unable to show API documentation.', 'Something went wrong!');
              });
        };
        
        // update active API page
        $scope.updatePage = function(versionId) {  
            
            $scope.activePage = $stateParams.page;
            if ($stateParams.page == undefined) {
                $scope.activePage = 'overview';
                $location.search('page', 'overview');
                $scope.loadOverview(versionId);
                return;
            }
                      
            switch ($stateParams.page) {
                case 'overview':
                    $scope.loadOverview(versionId);
                break;
                
                case 'details':
                    $scope.loadDocumentation(versionId);
                break;
            }
        };        
        
        // called when version has changed ... update active page details
        $scope.versionChanged = function(version) {
            $location.search('version', version.id);
            $scope.updatePage(version.id);
        };
    };
});