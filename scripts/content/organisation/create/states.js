define([
    'i18n!../../nls/strings', './create', 'text!./create.html'
], function (strings, organisationCreateController, organisationCreateTemplate) {
    return {
        id: 'create',
        url: "/create",
        title: strings.states.organisation.title,
        longTitle: strings.states.dashboard.longTitle,
        template: organisationCreateTemplate,
        controller: organisationCreateController
    };
});
