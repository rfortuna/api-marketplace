define([
], function () {

    return /*@ngInject*/ function ($scope, $state, Organisation, Country, User) {

        //to-do: change to localisation
        $scope.submitMessage = 'Create';
        $scope.headerMessage = 'Create organisation';

        //to-do: Will be later replaced with real user in root scope
        $scope.user = {
            id: 1,
            firstname: "Janez",
            lastname: "Novak",
            email: "janeznovak@gmail.com"
        };

        $scope.countries = Country.getCountries();

        $scope.createOrganisation = function(organisation){
            User.saveOrganisationWithOwner({id: $scope.user.id}, organisation).$promise
                .then(function(data) {
                    $state.go('^.list');
                });
        }


    };
});