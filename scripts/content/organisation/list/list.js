define([
], function () {

    return /*@ngInject*/ function ($scope, Organisation) {

        $scope.radioFilterModel = 'All';
        $scope.radioLayoutModel = 'Grid';

        Organisation.getAll().$promise
            .then(function(data) {
                $scope.organisations = data;
            });

    };
});