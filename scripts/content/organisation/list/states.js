define([
    'i18n!../../nls/strings', './list', 'text!./list.html'
], function (strings, organisationListController, organisationListTemplate) {
    return {
        id: 'list',
        url: "/list",
        title: strings.states.organisation.title,
        longTitle: strings.states.dashboard.longTitle,
        template: organisationListTemplate,
        controller: organisationListController
    };
});
