define([
    'i18n!../nls/strings', './organisation', 'text!./organisation.html', './list/states', './create/states', './edit/states', './details/states'
], function (strings, organisationController, organisationTemplate, listStates, createStates, editStates, detailsState) {
    return {
        id: 'organisation',
        url: "/organisation",
        title: strings.states.organisation.title,
        longTitle: strings.states.dashboard.longTitle,
        template: organisationTemplate,
        controller: organisationController,
        children: [listStates, createStates, editStates, detailsState]
    };
});
