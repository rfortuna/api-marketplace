define([
], function () {

    return /*@ngInject*/ function ($scope, $state, $stateParams, toastr, Organisation, Country, User) {



        //to-do: Will be later replaced with real user in root scope
        $scope.user = {
            id: 1,
            firstname: "Janez",
            lastname: "Novak",
            email: "janeznovak@gmail.com"
        };

        Organisation.get({id: $stateParams.organisationId}).$promise
            .then(function(data) {
                $scope.organisation = data;
                $scope.canEdit = true;

                return Country.get({id: $scope.organisation.country.id}).$promise

            }).then(function (data) {
                $scope.organisation.country = data;

            }).catch(
                function(){
                    toastr.error('Unable to load Organisation.', 'Something went wrong!');
                    $state.go('^.list');
                }

            );



    };
});