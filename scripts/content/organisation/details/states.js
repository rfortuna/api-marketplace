define([
    'i18n!../../nls/strings', './details', 'text!./details.html'
], function (strings, organisationDetailsController, organisationDetailsTemplate) {
    return {
        id: 'details',
        url: "/details/{organisationId}",
        title: strings.states.organisation.title,
        longTitle: strings.states.dashboard.longTitle,
        template: organisationDetailsTemplate,
        controller: organisationDetailsController
    };
});
