define([
], function () {

    return /*@ngInject*/ function ($scope, $state, $stateParams, toastr, Organisation, Country, User) {


        //to-do: Will be later replaced with real user in root scope
        $scope.user = {
            id: 1,
            firstname: "Janez",
            lastname: "Novak",
            email: "janeznovak@gmail.com"
        };

        Organisation.get({id: $stateParams.organisationId}).$promise
            .then(function(data) {
                $scope.organisation=data;
                $scope.countries = Country.getCountries();
            },
            function(){
                toastr.error('Unable to load Organisation.', 'Something went wrong!');
                $state.go('^.list');
            });


        $scope.updateOrganisation = function(organisation){

            if($scope.organisation !== undefined){
                Organisation.update({id: $scope.organisation.id}, organisation).$promise
                    .then(function(data) {
                        $state.go('^.list');
                    });
            }
            else
            {
                toastr.error('Unable to update Organisation.', 'Something went wrong!');

            }
        };


    };
});