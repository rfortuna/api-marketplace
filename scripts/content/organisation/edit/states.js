define([
    'i18n!../../nls/strings', './edit', 'text!./edit.html'
], function (strings, organisationEditController, organisationEditTemplate) {
    return {
        id: 'edit',
        url: "/edit/{organisationId}",
        title: strings.states.organisation.title,
        longTitle: strings.states.dashboard.longTitle,
        template: organisationEditTemplate,
        controller: organisationEditController
    };
});
