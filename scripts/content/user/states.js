﻿define([
    'i18n!../nls/strings', './user', 'text!./user.html',
    './apis/states', './applications/states', './organisations/states'
], function (strings, profileController, profileTemplate,
             apisStates, applicationsStates, organisationsStates) {
    return {
        id: 'user',
        url: '/user',
        abstract: true,
        visible: false,
        title: strings.states.user.title,
        longTitle: strings.states.user.longTitle,
        template: profileTemplate,
        controller: profileController,
        children: [
            apisStates,
            applicationsStates,
            organisationsStates
        ],
        menuItems: [
            {
                name: 'My APIs',
                link: 'layout.user.apis'
            },
            {
                name: 'My Applications',
                link: 'layout.user.applications'
            },
            {
                name: 'My Organisations',
                link: 'layout.user.organisations'
            }
        ]
    };
});
