﻿define([
    'i18n!../../nls/strings', './applications', 'text!./applications.html'
], function (strings, applicationsController, applicationsTemplate) {
    return {
        id: 'applications',
        url: '/applications',
        title: strings.states.user.applications.title,
        longTitle: strings.states.user.applications.longTitle,
        template: applicationsTemplate,
        controller: applicationsController
    };
});
