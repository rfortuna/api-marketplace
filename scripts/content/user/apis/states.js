﻿define([
    'i18n!../../nls/strings', './apis', 'text!./apis.html'
], function (strings, apisController, apisTemplate) {
    return {
        id: 'apis',
        url: '/apis',
        title: strings.states.user.apis.title,
        longTitle: strings.states.user.apis.longTitle,
        template: apisTemplate,
        controller: apisController
    };
});
