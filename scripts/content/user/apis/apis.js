﻿define([
], function () {

    return /*@ngInject*/ function ($scope, User) {

    	User.getProducers({ id: $scope.user.id }).$promise
            .then(function(data) {
                $scope.producers = data;
                console.log($scope.producers);
            });

    };
});