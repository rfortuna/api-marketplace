﻿define([
], function () {

    return /*@ngInject*/ function ($scope, User) {

        User.getOrganisations({ id: $scope.user.id }).$promise
            .then(function(data) {
                $scope.organisations = data;
            });

    };
});