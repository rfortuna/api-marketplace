﻿define([
    'i18n!../../nls/strings', './organisations', 'text!./organisations.html'
], function (strings, organisationsController, organisationsTemplate) {
    return {
        id: 'organisations',
        url: '/organisations',
        title: strings.states.user.organisations.title,
        longTitle: strings.states.user.organisations.longTitle,
        template: organisationsTemplate,
        controller: organisationsController
    };
});
