define([], function () {

    return {
        urls: {
            api: '<%- api %>' || "http://localhost:8002",
            keycloak: '<%- keycloak %>' || "http://localhost:8000/auth/admin/realms",
        },
        realmName:	'api-marketplace',
        version: '<%- version %>'
    };
});
