define([
    'text!./profile.html'
], function (profile) {

  return function ()  {

    return {
      restrict: 'E',
      replace: true,
      template: profile,
      scope: {
        user: '=',
        readOnly: '='
      },
      controller: function($rootScope, $scope, toastr, User) {
        $scope.editable = false;
        $scope.passwordEdit = false;
        console.log($scope.user);
        //$scope.user.name = $scope.user.firstName + " " + $scope.user.lastName; 
          
        $scope.mode = function(){
          $scope.editable = !$scope.editable;

          // Create backup for user before editing
          if($scope.editable){
            $scope.userBackup = angular.copy($scope.user);
          // If editing canceled restore all changes
          }else{ 
            $scope.user = $scope.userBackup;
          }

        }
        
        $scope.passwordMode = function(){
          $scope.passwordEdit = !$scope.passwordEdit;
        }

        $scope.updateUser = function(){
          User.update({username: $scope.user.username},$scope.user).$promise
            .then(function (data){
              toastr.success("Profile successfuly updated.");
              $scope.editable = false;
            });
        }

        $scope.resetPassword = function(){
          
          // Quick chech for passwords..must do better
          if($scope.user.password == $scope.user.passwordRepeat && $scope.user.password != null && $scope.user.passwordRepeat != null){
            User.resetPassword({username:$scope.user.username},{type : "password", value : $scope.user.password,temporary: "false"})
            .$promise
            .then(function (data) {
              toastr.success("Password successfuly changed.");
              $scope.passwordEdit = false;
            });
          }else{
              toastr.error("Passwords must match.");
              $scope.user.password = "";
              $scope.user.passwordRepeat = "";
          }
        }
      }
    }
  }
});