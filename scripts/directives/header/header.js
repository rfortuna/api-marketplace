define([
    'text!./header.html'
], function (header) {

    return function () {

        return {
            restrict: 'E',
            replace: true,
            template: header,
            controller: function($rootScope, $scope) {
                // Check if user is logged in
      			$scope.logged = $rootScope.auth != undefined && $rootScope.auth.authenticated;
                  
                 $scope.refreshAPIs = function(){
                     console.log("refresh search");
                 };
                 
                $scope.categories = [];
        
                /*Category.query().$promise.then(function(data){
                    $scope.categories = data;
                });*/
                
                 $scope.apis = [
                     {
                         id: 0,
                         name: "Res 2 api"
                     },
                     {
                         id: 1,
                         name: "BicikeLJ"
                     },
                     {
                         id:2,
                         name: "REST APi LJ"
                     }
                 ];

            }
        }
    }
});