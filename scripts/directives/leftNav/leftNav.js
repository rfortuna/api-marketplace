define([
    'text!./leftNav.html', 'metis-menu'
], function (leftNav) {

    return /*@ngInject*/ function ($state, $compile, $timeout) {

        return {
            restrict: 'E',
            replace: true,
            template: leftNav,
            link: function(scope, element){

                scope.menuItems = function() {

                    var findMenu = function(state) {
                        var $body = $('body');
                        var $minimalizeMenu = $('minimalize-menu');

                        if (state === undefined) {

                            $body.addClass("hide-sidebar")

                            $minimalizeMenu.addClass("hide");

                            return [];
                        }

                        if (state.menuItems) {

                            $body.removeClass("hide-sidebar");

                            $minimalizeMenu.removeClass("hide");

                            return state.menuItems;
                        }

                        return findMenu(state.parentState)
                    };

                    return findMenu($state.current);
                };

                element.html($compile(leftNav)(scope));

                $timeout(function() {
                    scope.$apply();
                    element.metisMenu();
                });
            },
            /*
            controller: function ($scope, $timeout) {

                $scope.menuitems = [
                    {
                        name: 'overview',
                        id: 'someId4',
                        children: [
                            {
                                id: 'someId1',
                                name: 'overview1',
                                children: [
                                    {
                                        id: 'someId2',
                                        name: 'subgroup1.1'
                                    },
                                    {
                                        id: 'someId3',
                                        name: 'subgroup1.2'
                                    }
                                ]
                            },
                            {
                                id: 'someId5',
                                name: 'overview2'
                            }
                        ]
                    },
                    {
                        id: 'apiDetails',
                        name: 'API Details',
                        active: true,
                        scrollspy: true,
                        children: [
                            {
                                id: 'someId8',
                                name: 'group1',
                                children: [
                                    {
                                        id: 'someId9',
                                        name: 'subgroup1.1'
                                    },
                                    {
                                        id: 'someId10',
                                        name: 'subgroup1.2'
                                    }
                                ]
                            },
                            {
                                id: 'someId11',
                                name: 'group2',
                                children: [
                                    {
                                        id: 'someId12',
                                        name: 'subgroup2.1'
                                    },
                                    {
                                        id: 'someId13',
                                        name: 'subgroup2.1'
                                    }
                                ]
                            }
                        ]
                    },
                    {
                        id: 'someId14',
                        name: 'Location'
                    }
                ];

                angular.forEach($scope.menuItems, function (menuItem) {
                    if (menuItem.markdown) {
                        menuItem.children = [];


                    }
                });

                $(document).ready(function () {
                    $('body').attr('data-offset', $('#header').height());

                    $timeout(function () {
                        $('#side-menu').metisMenu();
                    });

                });

                var correctAnchorScrolling = function () {
                    $('.nav li a').click(function (event) {
                        event.preventDefault();

                        var href = $(this).attr('href');
                        if (!$(href).length)
                            return;

                        $(href)[0].scrollIntoView();

                        var headerOffset = $('#header').height() - 1;
                        scrollBy(0, -headerOffset);

                        //refreshScrollSpy();
                    });
                };

                var refreshSideMenu = function () {
                    // side menu refresh
                    $('#side-menu').removeData('plugin_metisMenu').metisMenu();
                };

                var refreshScrollSpy = function () {
                    // scroll spy refresh
                    $('[data-spy="scroll"]').each(function () {
                        $(this).scrollspy('refresh')
                    });
                };

                //var markdown2HTML = function(preview) {
                //	var markdown = $('#markdown-input').val();
                //
                //	var converter = new showdown.Converter({
                //		//extensions: ['table']
                //	});
                //	var html = converter.makeHtml(markdown);
                //
                //	$('#html-output').html(html);
                //
                //	if (preview)
                //		return;
                //
                //	var nav = generateNavigation(html);
                //	if (nav) {
                //		$('#api-details-nav').find('ul').remove();
                //		$('#api-details-nav').append(nav);
                //
                //		refreshSideMenu();
                //		refreshScrollSpy();
                //		correctAnchorScrolling();
                //	}
                //}

                var generateNavigation = function (html) {
                    var $dom = $('<div />').append($(html));

                    return secondLevelTemplate($dom);
                };

                var secondLevelTemplate = function ($dom) {
                    var $h1Elements = $dom.find('h1');

                    if (!$h1Elements || $h1Elements.length == 0)
                        return "";

                    var template = '<ul class="nav nav-second-level">';

                    $h1Elements.each(function () {
                        template += '<li>';

                        template += '<a href="#' + $(this).attr('id') + '">';
                        template += '<span class="nav-label">' + $(this).text() + '</span><span class="fa arrow"></span></a>';
                        template += thirdLevelTemplate($(this).nextUntil('h1', 'h2'));

                        template += '</li>';
                    });

                    template += '</ul>';

                    return template;
                };

                var thirdLevelTemplate = function ($h2Elements) {
                    if (!$h2Elements || $h2Elements.length == 0)
                        return "";

                    var template = '<ul class="nav nav-third-level">'

                    $h2Elements.each(function () {
                        template += '<li>';
                        template += '<a href="#' + $(this).attr('id') + '">' + $(this).text() + '</a>';
                        template += '</li>';
                    });

                    template += '</ul>';

                    return template;
                };

                $("body").on('activate.bs.scrollspy', function (element) {
                    // do something…
                    console.log("bla");
                });

                var menuElement = $('#side-menu').find('a:not([href$="\\#"])');
                menuElement.click(function () {

                    if ($(window).width() < 769) {
                        $("body").toggleClass("show-sidebar");
                    }
                });
            }*/
        }
    };
});