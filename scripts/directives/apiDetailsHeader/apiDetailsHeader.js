define([
    'text!./apiDetailsHeader.html'
], function (apiDetailsHeader) {

    return function () {

        return {
            restrict: 'E',
            replace: true,
            template: apiDetailsHeader,
            scope: {
                apiDetails: '=',
                initialVersion: '=',
                versionCallback: '&versionChanged'
            },
            controller: function($scope) {
                
                // placeholder to be used when producer is missing logo
                $scope.placeholderLogo = 'http://www.warwickrowers.org/wp-content/uploads/2014/10/default-placeholder-200x200.png';   
                $scope.$watch('apiDetails', function() {
                   if ($scope.apiDetails != undefined && $scope.apiDetails.logo == undefined) {
                       $scope.apiDetails.logo = $scope.placeholderLogo;
                   } 
                });           
               
                // redirect callback to main controller
                $scope.versionChanged = function(version) {
                  $scope.versionCallback({
                      version: version
                  });  
                };
            }
        }
    }
});