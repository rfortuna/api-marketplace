define([
    'text!./apiVersionSelect.html'
], function (apiVersionSelect) {

    return function () {

        return {
            restrict: 'E',
            replace: true,
            template: apiVersionSelect,
            scope: {
                changeCallback: '&versionChanged',
                versions: '=',
                initialVersion: '='
            },
            controller: function ($scope) {
                $scope.$watch('versions', function() {
                    angular.forEach($scope.versions, function (version) {
                        
                        // select default version if no initial version is defined
                        if (version.isDefault && $scope.initialVersion == undefined) {
                            $scope.selectedVersion = version;
                            $scope.changeCallback({
                               version: version
                            });
                        }
                        
                        // set to initial version if defined
                        else if ($scope.initialVersion != undefined && 
                            $scope.initialVersion == version.id) {
                            $scope.selectedVersion = version;
                            $scope.changeCallback({
                               version: version
                            });
                        }
                    });
                });

                $scope.valueChanged = function (version) {
                    $scope.changeCallback({
                        version: version
                    });
                };
            }
        }
    }
});