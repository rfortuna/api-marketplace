define([
    'text!./announcementList.html'
], function (announcementList) {

    return function () {

        return {
            restrict: 'E',
            replace: true,
            template: announcementList,
            scope: {
                producerId: '='
            },
            controller: function($scope, $filter, Producer, toastr) {
                
                // load list of announcements
                Producer.listAnnouncements({id: $scope.producerId}).$promise.then(function(data) {
                  $scope.announcements = data;
                }, function() {
                  toastr.error('Unable to load announcements.', 'Something went wrong!');
                });                
                
                // react to add button click
                $scope.createAnnouncementClick = function() {
                    $scope.createMode = true;
                };
                
                // react to cancel button click
                $scope.createCancelled = function() {
                  $scope.createMode = false;
                };
                
                // react to added announcement
                $scope.announcementAdded = function(id) {
                    Producer.getAnnouncement({id: $scope.producerId, id2: id}).$promise.then(
                        function(data) {
                            $scope.announcements.unshift(data);
                            $scope.createMode = false;
                        }, function() {
                            
                        }
                    );
                };
                
                // react to removal requests
                $scope.delete = function(announcement) {
                    console.log(announcement);
                    Producer.removeAnnouncement({id: $scope.producerId, id2: announcement.id}).$promise.then(
                        function(data) {
                            $scope.announcements = $filter('filter')
                            ($scope.announcements, function(value, index) {
                                return value.id != announcement.id;
                            });
                        }, function() {
                            toastr.error('Failed to remove announcement.', 'Something went wrong!');
                        }
                    );
                };
            }
        }
    }
});