define([
    'text!./createAnnouncement.html'
], function (createAnnouncement) {

    return function () {

        return {
            restrict: 'E',
            replace: true,
            template: createAnnouncement,
            scope: {
                cancelCallback: '&onCancel',
                doneCallback: '&onCreateComplete',
                producerId: '='
            },
            controller: function($scope, Producer, toastr) {
                
                // form data
                $scope.formData = {
                    title: "",
                    text: ""
                };
                
                // pass cancel click to parent
                $scope.cancelCreate = function() {
                    $scope.cancelCallback();
                };
                
                // create announcement
                $scope.submitForm = function() {
                    Producer.addAnnouncement({id: $scope.producerId}, {
                        description: $scope.formData.text,
                        title: $scope.formData.title,
                        producer: {
                            id: $scope.producerId
                        }
                    }).$promise.then(function(data) {
                        $scope.formData.title = "";
                        $scope.formData.text = "";
                        $scope.doneCallback({
                            id: data.id
                        });
                    }, function() {
                        toastr.error('Failed to add new announcement.', 'Something went wrong!');
                    });
                };
                
                
            }
        }
    }
});