define([
    'text!./form.html'
], function (template) {

    return function () {

        return {
            restrict: 'E',
            replace: true,
            template: template,
            scope: {
                organisation: '=',
                countries: '=',
                submitOrganisation: '&',
                headerMessage:'=',
                submitMessage:'='
            },
            controller: function($scope, $rootScope){
                $scope.$strings = $rootScope.$strings;

            }
        }
    }
});