define([
    'text!./grid.html'
], function (template) {

    return function () {

        return {
            restrict: 'E',
            replace: true,
            template: template,
            controller: function(){
                
            }
        }
    }
});