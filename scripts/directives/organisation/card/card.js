define([
    'text!./card.html'
], function (card) {

    return /*@ngInject*/ function ($state) {

        return {
            restrict: 'E',
            replace: true,
            template: card,
            controller: function($scope){

                $scope.goToDetails = function() {
                    $state.go('layout.organisation.details', {organisationId: $scope.organisation.id})
                }
            }
        }
    }
});