define([
    'text!./categoryNav.html'
], function (categoryNav) {

    return function () {

        return {
            restrict: 'E',
            replace: true,
            template: categoryNav,
            scope: {},
            controller: /*@ngInject*/ function ($scope) {
                //Codelists.one("category").getList().then(function (categories) {
                //    $scope.categories = categories;
                //});

                // TODO  - fill from service
                /*
                 $scope.categories = [
                 {
                 description: 'Money',
                 },
                 {
                 description: 'Weather',
                 },
                 {
                 description: 'Transport',
                 },
                 ];
                 */
            }
        }
    };
});