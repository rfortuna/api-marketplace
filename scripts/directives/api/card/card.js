define([
    'text!./card.html'
], function (card) {

    return /*@ngInject*/ function ($state) {

        return {
            restrict: 'E',
            replace: true,
            template: card,
            controller: function($scope){
                $scope.viewingCategories = false;

                $scope.showCategories = function(){
                    console.log("show categories");
                    $scope.viewingCategories = true;
                }

                $scope.hideCategories = function(){
                    console.log("hide categories");
                    $scope.viewingCategories = false;
                }

            }
        }
    }
});
