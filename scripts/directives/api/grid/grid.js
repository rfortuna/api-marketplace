define([
    'text!./grid.html'
], function (template) {

    return /*@ngInject*/ function ($state) {

        return {
            restrict: 'E',
            replace: true,
            template: template,
            controller: function($scope){
            }
        }
    }
});
