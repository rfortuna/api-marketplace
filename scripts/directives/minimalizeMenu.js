define([], function () {

    return function () {
        return {
            restrict: 'EA',
            template: '<div class="header-link hide-menu" ng-click="minimalize()"><i class="fa fa-bars"></i></div>',
            controller: /*@ngInject*/ function ($scope) {

                var $body = $('body');

                $scope.minimalize = function () {

                    if ($(window).width() <= 768) {
                        $body.toggleClass("show-sidebar");
                    } else {
                        $body.toggleClass("hide-sidebar")
                             .removeClass("show-sidebar");
                    }
                }
            }
        };
    };
});