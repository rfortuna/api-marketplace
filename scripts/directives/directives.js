define([
    'lodash',
    './icheck',
    './minimalizeMenu',
    './panelTools/panelTools',
    './smallHeader',
    './leftNav/leftNav',
    './apiDetailsHeader/apiDetailsHeader',
    './apiVersionSelect/apiVersionSelect',
    './categoryNav/categoryNav',
    './header/header',
    './api/grid/grid',
    './api/card/card',
    './organisation/grid/grid',
    './organisation/card/card',
    './organisation/form/form',
    './announcements/list/announcementList',
    './announcements/create/createAnnouncement',
    './profile/profile',
    './application/card/card',
    './fileUpload'
], function (
    _,
    icheck,
    minimalizeMenu,
    panelTools,
    smallHeader,
    leftNav,
    apiDetailsHeader,
    apiVersionSelect,
    categoryNav,
    header,
    grid,
    card,
    organisationGrid,
    organisationCard,
    organisationForm,
    announcementList,
    createAnnouncement,
    profile,
    applicationCard,
    fileUpload
) {
    var directives = {
        icheck: icheck,
        minimalizeMenu: minimalizeMenu,
        panelTools: panelTools,
        smallHeader: smallHeader,
        leftNav: leftNav,
        apiCard: card,
        organisationCard: organisationCard,
        apiDetailsHeader: apiDetailsHeader,
        apiVersionSelect: apiVersionSelect,
        categoryNav: categoryNav,
        pageHeader: header,
        apiGrid: grid,
        organisationGrid: organisationGrid,
        organisationForm: organisationForm,
        announcementList: announcementList,
        createAnnouncement: createAnnouncement,
        profile: profile,
        applicationCard: applicationCard,
        fileUpload: fileUpload
    };

    var initialize = function (angModule) {
        angular.forEach(directives, function (directive, name) {
            angModule.directive(name, directive);
        });
    };

    return {
        initialize: initialize
    };
});