define([
    'text!./card.html'
], function (card) {
    return /*@ngInject*/ function ($state) {
        return {
            restrict: 'E',
            replace: true,
            template: card,
            controller: function($scope){
                $scope.goToDetails = function() {
                    $state.go('layout.application.details', {applicationId: $scope.application.id})
                }
            }
        }
    }
});
