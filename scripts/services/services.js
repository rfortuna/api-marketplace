﻿define([
    'lodash'
], function (_) {

    var services = {};

    var initialize = function (angModule) {
        _.forEach(services, function (service, name) {
            angModule.service(name, service);
        });
    };

    return {
        initialize: initialize
    };
});