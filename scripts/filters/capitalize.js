define( [
  'lodash'
], function ( _ ) {
  return function () {

    return function ( input ) {

      if ( input != null )
        input = input.toLowerCase();
      else
        return '';

      var inputs = _( input.split( ' ' ) ).map( function ( i ) {
        return i.substring( 0, 1 ).toUpperCase() + i.substring( 1 );
      } );

      return inputs.join( ' ' );
    }
  };
} );