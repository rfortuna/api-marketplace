﻿define( [
  'lodash', './capitalize', './shortenText'
], function ( _, capitalize, shortenText ) {

  var filters = {
    capitalize: capitalize,
    shortenText: shortenText
  };

  var initialize = function ( angModule ) {
    _.forEach( filters, function ( filter, name ) {
      angModule.filter( name, filter );
    } );
  };
  return {
    initialize: initialize
  };
} );