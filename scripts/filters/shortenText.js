define( [
  'lodash'
], function ( _ ) {
  return function () {

    return function ( input, limit ) {
      if (input === undefined)
        return "";

      return input.length > limit
        ? input.substring(0, limit - 1) + "..."
        : input;
    }
  };
} );