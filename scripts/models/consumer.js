define([], function () {

    return /*@ngInject*/ function ($resource, $config) {
        /*
        return $resource(
            $config.urls.api + '/producers/:id/:resource',
            {
                id: '@id',
                resource: '@resource',
            },
            {
                getAll: {
                    method: 'GET',
                    url: $config.urls.api + '/producers/list',
                    isArray: true
                },
                getApiDetails: {
                    method: 'GET',
                    params: {
                        resource: ''
                    }
                },
                getVersions: {
                    method: 'GET',
                    params: {
                        resource: 'versions'
                    },
                    isArray: true
                },
                getOverview: {
                    method: 'GET',
                    params: {
                        resource: 'overview'
                    }
                },
                getDetails: {
                    method: 'GET',
                    params: {
                        resource: 'details'
                    },
                    isArray: true
                },
                getCategories: {
                    method: 'GET',
                    params: {
                        resource: 'categories'
                    },
                    isArray: true
                },
                getAnnouncements: {
                    method: 'GET',
                    params: {
                        resource: 'announcements'
                    },
                    isArray: true
                }
            }
        );
        */
    };
});