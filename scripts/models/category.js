define([], function () {

    return /*@ngInject*/ function ($resource, $config) {

    	return $resource(
			$config.urls.api + '/categories',
			{},
			{
				getCategories: {
                    method: "GET",
                }
			}
		);
    };
});