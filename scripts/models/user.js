define([], function () {

    return /*@ngInject*/ function ($resource, $config, $rootScope) {

    	return $resource(
			$config.urls.api + '/users/:id/:resource',
			{
				id: '@id',
				resource: '@resource',
                username: '@username'
			},
			{
				getOrganisations: {
                    method: 'GET',
                    params: {
                        resource: 'organisations'
                    },
                    isArray: true
                },
				getConsumers: {
                    method: 'GET',
                    params: {
                        resource: 'consumers'
                    },
                    isArray: true
                },
				getProducers: {
                    method: 'GET',
                    params: {
                        resource: 'producers'
                    },
                    isArray: true
                },
                saveOrganisationWithOwner: {
                    method: 'POST',
                    params:{
                        resource: 'organisations'
                    }
                },
                createConsumer: {
                    method: 'POST',
                    params: {
                        resource: 'consumers'
                    }
                },
                // Requests on keycloak server
                all:{
                    url: $config.urls.keycloak + '/:realmname/users',
                    headers: {
                        Authorization: 'Bearer '+$rootScope.$auth.keycloak.token
                    },
                    params:{
                        realmname: $config.realmName
                    },
                    isArray: true
                },
                search:{
                    url: $config.urls.keycloak + '/:realmname/users?lastName=:filter.lastName' +
                    '&firstName=:filter.firstName&email=:filter.email&username=:filter.username&first=:filter.firstName&max=:filter.max',
                    headers: {
                        Authorization: 'Bearer '+$rootScope.$auth.keycloak.token
                    },
                    params:{
                        realmname: $config.realmName
                    },
                    isArray: true
                },
                update:{
                    method: 'PUT',
                    url: $config.urls.keycloak + '/:realmname/users/:username',
                    headers: {
                        Accept: "application/json",
                        Authorization: 'Bearer '+$rootScope.$auth.keycloak.token,
                    },
                    params: {
                        realmname: $config.realmName
                    }  
                },
                resetPassword:{
                    method: 'PUT',
                    url: $config.urls.keycloak + '/:realmname/users/:username/:resource',
                    headers: {
                        Accept: "application/json",
                        Authorization: 'Bearer '+$rootScope.$auth.keycloak.token,
                    },
                    params: {
                        realmname: $config.realmName,
                        resource : 'reset-password'   
                    }
                }
			}
		);
    };
});