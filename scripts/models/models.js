define([
    'lodash', './producer', './user', './organisation', './category',
    './country', './versionType', './consumer', './publish'
], function (_, Producer, User, Organisation, Category,
             Country, VersionType, Consumer, Announcement, Publish) {

    var models = {
        Producer: Producer,
        User: User,
        Category: Category,
        Organisation: Organisation,
        Country: Country,
        VersionType: VersionType,
        Publish: Publish
    };

    var initialize = function (angModule) {
        _.forEach(models, function (model, name) {
            angModule.factory(name, model);
        });
    };

    return {
        initialize: initialize
    };
});