define([], function () {

    return /*@ngInject*/ function ($resource, $config) {

        return $resource(
            $config.urls.api + '/organisations/:id',
            {
                id: '@id'
            },
            {
                getAll: {
                    method: 'GET',
                    url: $config.urls.api + '/organisations/list',
                    isArray: true
                },
                update: {
                    method: 'PUT'
                }
            }
        );
    };
});