define([], function () {

    return /*@ngInject*/ function ($resource, $config) {

        return $resource(
            $config.urls.api + '/users/:id/producer',
            {
            	id: '@id'
            },
            {
                createApi: {
                    method: 'POST',
                }
            }
        );
    };
});