define([], function () {

    return /*@ngInject*/ function ($resource, $config) {

        return $resource(
            $config.urls.api + '/producers/:id/:resource/:id2',
            {
                id: '@id',
                resource: '@resource',
                id2: '@id2'
            },
            {
                getAll: {
                    method: 'GET',
                    url: $config.urls.api + '/producers/list',
                    isArray: true
                },
                getApiDetails: {
                    method: 'GET',
                    params: {
                        resource: ''
                    }
                },
                getVersions: {
                    method: 'GET',
                    params: {
                        resource: 'versions'
                    },
                    isArray: true
                },
                getOverview: {
                    method: 'GET',
                    params: {
                        resource: 'overview'
                    }
                },
                getDetails: {
                    method: 'GET',
                    params: {
                        resource: 'details'
                    },
                    isArray: true
                },
                getCategories: {
                    method: 'GET',
                    params: {
                        resource: 'categories'
                    },
                    isArray: true
                },
                listAnnouncements: {
                    method: 'GET',
                    params: {
                        resource: 'announcements'
                    },
                    isArray: true
                },
                addAnnouncement: {
                    method: 'POST',
                    params: {
                        resource: 'announcements'                   
                    }                     
                },
                getAnnouncement: {
                    method: 'GET',
                    params: {
                        resource: 'announcements'
                    }
                },
                removeAnnouncement: {
                    method: 'DELETE',
                    params: {
                        resource: 'announcements'
                    }
                }
            }
        );
    };
});