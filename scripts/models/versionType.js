define([], function () {

    return /*@ngInject*/ function ($resource, $config) {

        return $resource(
            $config.urls.api + '/versionTypes/:id',
            {
                id: '@id'
            },
            {
                getAll: {
                    method: "GET",
                    isArray: true
                }
            }
        );
    };
});