define([], function () {

    return /*@ngInject*/ function ($resource, $config) {

        return $resource(
            $config.urls.api + '/countries/:id',
            {
                id: '@id'

            },
            {
                getCountries: {
                    method: "GET",
                    isArray: true
                }
            }
        );
    };
});