define([], function () {

  return /*@ngInject*/ function ( $q ) {

    return {
      request: function ( config ) {
        var deferred = $q.defer();

        deferred.resolve(config);

        return deferred.promise;
      }
    };
  };
} );