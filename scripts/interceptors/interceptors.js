define([
  'lodash', './auth', './errors'
], function ( _, auth, errors ) {

  var interceptors = {
    authInterceptor: auth,
    errorInterceptor: errors
  };

  var initialize = function ( angModule ) {
    _.forEach( interceptors, function ( interceptor, name ) {
      angModule.factory( name, interceptor );
    } );
  };

  return {
    initialize: initialize
  };
} );