define([
    'lodash'
], function (_) {

    return /*@ngInject*/ function ($q, $config) {

        return {
            responseError: function (rejection) {

                if (rejection.status == 401) {


                } else if (rejection.status == 0) {


                } else if (rejection.status == 403) {


                } else if (!_.contains($config.passErrors, rejection.status)) {

                }

                return $q.reject(rejection);
            }
        };
    };
});