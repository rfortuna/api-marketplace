﻿require.config({
    locale: 'en',
    waitSeconds: 0,
    paths: {
        'jquery': '../vendor/jquery/dist/jquery',
        'lodash': '../vendor/lodash/lodash',
        'i18n': '../vendor/requirejs-i18n/i18n',
        'text': '../vendor/requirejs-text/text',
        'angular': '../vendor/angular/angular',
        'angular-ui-router': '../vendor/angular-ui-router/release/angular-ui-router',
        'angular-sanitize': '../vendor/angular-sanitize/angular-sanitize',
        'angular-animate': '../vendor/angular-animate/angular-animate',
        'angular-ui-utils': '../vendor/angular-ui-utils/ui-utils.min',
        'angular-bootstrap': '../vendor/angular-bootstrap/ui-bootstrap-tpls',
        'angular-toastr': '../vendor/angular-toastr/dist/angular-toastr.tpls',
        'angular-gravatar': '../vendor/angular-gravatar/build/angular-gravatar',
        'angular-resource': '../vendor/angular-resource/angular-resource',
        'angular-infinite-scroll': '../vendor/ngInfiniteScroll/build/ng-infinite-scroll.min',
        'angular-ui-select' : '../vendor/angular-ui-select/dist/select',
        'ngTable': '../vendor/ng-table/dist/ng-table',
        'sweetalert': '../vendor/sweetalert/dist/sweetalert-dev',
        'icheck': '../vendor/iCheck/icheck',
        'metis-menu': '../vendor/metisMenu/dist/metisMenu.min',
        'config': './config',
        'keycloak': '../vendor/keycloak/dist/keycloak',
        'showdown': '../vendor/showdown/compressed/Showdown.min',
        'markdown': '../vendor/angular-markdown-directive/markdown'
        
    },
    shim: {
        'angular': {
            exports: 'angular',
            deps: ['jquery']
        },
        'angular-ui-router': {
            deps: ['angular']
        },
        'angular-sanitize': {
            deps: ['angular']
        },
        'angular-animate': {
            deps: ['angular']
        },
        'angular-ui-utils': {
            deps: ['angular']
        },
        'angular-bootstrap': {
            deps: ['angular']
        },
        'angular-toastr': {
            deps: ['angular']
        },
        'angular-gravatar': {
            deps: ['angular']
        },
        'angular-resource': {
            deps: ['angular']
        },
        'angular-infinite-scroll': {
            deps: ['angular','jquery']  
        },
        'angular-ui-select' :{
            deps: ['angular']
        },
        'ngTable': {
            deps: ['angular']
        },
        'metis-menu': {
            deps: ['jquery']
        },
        'icheck': {
            deps: ['jquery']
        },
        'markdown': {
            deps: ['angular-sanitize', 'showdown']
        }
    }
});

require([
    'app'
], function (App) {
    App.initialize();
});
