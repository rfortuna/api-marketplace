define([
    'angular', 'angular-ui-router', './routing/stateTree', 'models/models', 'interceptors/interceptors',
    'filters/filters', 'services/services', 'directives/directives', 'factories/factories',
    'i18n!content/nls/strings',

    'angular-sanitize', 'angular-animate', 'angular-ui-utils', 'angular-bootstrap', 'angular-toastr',
    'angular-gravatar', 'angular-resource', 'angular-infinite-scroll', 'ngTable', 'markdown',
    'angular-ui-select', 'keycloak'
    
], function (angular, router, stateTree, models, interceptors,
             filters, services, directives, factories,
             strings) {

    var initialize = function () {

        var app = angular.module('api-marketplace', [
            'ui.router',
            'ui.bootstrap',
            'ui.select',
            'ngTable',
            'ngSanitize',
            'ngAnimate',
            'ngResource',
            'ui.utils',
            'ui.gravatar',
            'toastr',
            'infinite-scroll',
            'btford.markdown'
        ]);

        var auth = {};

        app.config(
            function ($stateProvider, $urlRouterProvider, $locationProvider, $httpProvider) {
                $locationProvider.html5Mode(true).hashPrefix('!');
                $urlRouterProvider.when('', '/');
                stateTree.addStates($stateProvider);
                //$urlRouterProvider.otherwise( '/404' );
                $httpProvider.interceptors.push('authInterceptor');
                $httpProvider.interceptors.push('errorInterceptor');
                $httpProvider.defaults.headers.common.Accept = 'application/json, text/plain';
                $httpProvider.defaults.headers.common['Content-Type'] = 'application/json';
                $httpProvider.defaults.timeout = 15000;
            });

        models.initialize(app);
        filters.initialize(app);
        services.initialize(app);
        factories.initialize(app);
        directives.initialize(app);
        interceptors.initialize(app);

        app.run(function ($rootScope, $state, $stateParams, toastr) {
            $rootScope.$state = $state;
            $rootScope.$stateTree = stateTree;
            $rootScope.$stateParams = $stateParams;
            $rootScope.$strings = strings;

            $rootScope.$auth = auth;
            $rootScope.loadings = {};

            $rootScope.isActiveState = function (stateid) {
                if ($rootScope.$state.current.hiddenSubPage) {
                    return $rootScope.$state.current.absoluteId.indexOf(stateid) === 0;
                }
                else return ($rootScope.$state.current.absoluteId === stateid);
            };

            $rootScope.hasActiveChild = function (state) {
                for (var i = 0; i < state.children.length; i++) {
                    var st = state.children[i];
                    if (st.children && st.children.length > 0) {
                        if ($rootScope.hasActiveChild(st)) return true;
                    }
                    else {
                        if ($rootScope.isActiveState(st.absoluteId)) return true;
                    }
                }
                return false;
            };

            $rootScope.getAbsoluteUrl = function (relativeUrl) {
                if (relativeUrl) {
                    return window.location.host + '/' + relativeUrl;
                }
            };

            // Authentication and authorization

            $rootScope.$on('$stateChangeStart',
                function (event, toState, toParams) {

                    // Check if state requires authentication and roles
                    if (toState.requiresAuth) {
                        // Check if user is logged in
                        if (auth.keycloak.authenticated) {
                            // Check ih user is allowed on that page
                            if (!isAllowed(toState.requiredRoles, auth.keycloak.realmAccess.roles[0])) {
                                $rootScope.$broadcast('noAuthorization');
                                event.preventDefault();
                            }
                            return;
                        }
                        $rootScope.$broadcast('noAuthentication');
                        event.preventDefault();
                    }
                });

            // Checks if user's role is allowed for state with required roles
            var isAllowed = function (requiredRoles, userRole) {

                // if requiresAuth no no specificRoles set allow all roles
                if(typeof (requiredRoles) == 'undefined'){
                   return true;   
                }
                
                for (r in requiredRoles) {
                    if (requiredRoles[r] == userRole) return true;
                }
                return false;
            };

            $rootScope.$on('noAuthentication', function () {

                if (auth.errorInit) {
                    $state.transitionTo('layout.dashboard');
                    auth.errorInit = false;
                } else {
                    auth.keycloak.login();
                }
            });

            $rootScope.$on('noAuthorization', function () {
                // Handle no roles (route to home, display error, ...)
                toastr.error(strings.errors.noAuthorization, 'Redirecting to dashboard');
                $state.transitionTo('layout.dashboard');
            });

            // Remove loading
            $('.splash').remove();
                 
          
        });

        auth.keycloak = new Keycloak();

        auth.keycloak.init({onLoad: 'check-sso'}).success(function () {
            angular.bootstrap(document, ['api-marketplace']);
        }).error(function () {
            auth.errorInit = true;
            angular.bootstrap(document, ['api-marketplace']);
        });
    };

    return {
        initialize: initialize
    };
});